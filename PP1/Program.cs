﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PP1
{
    class Program
    {
        static void Main(string[] args)
        {
            int n, a, b, nrValori = 0;
            double suma = 0, medie;
            Console.Write("Introduceti n: ");
            n = int.Parse(Console.ReadLine());
            double[] vector = new double[n];
            Console.Write("Introduceti a: ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Introduceti b: ");
            b = int.Parse(Console.ReadLine());
            Console.WriteLine("Introduceti " + n + " numere:");
            for (int i = 0; i < n; i++)
            {
                vector[i] = double.Parse(Console.ReadLine());
                if (vector[i] >= a && vector[i] <= b)
                {
                    nrValori++;
                    suma += vector[i];
                }
            }
            medie = suma / nrValori;
            Console.WriteLine("Media valorilor din vector cuprinse intre " + a + " si " + b + " este: " + medie );
            Console.ReadKey();

        }
    }
}
